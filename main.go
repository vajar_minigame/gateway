package main

import (
	"context" // Use "golang.org/x/net/context" for Golang version <= 1.6
	"flag"
	"net/http"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	gw_battle "gitlab.com/vajar_minigame/minigame_backend/battle/proto"
	gw_auth "gitlab.com/vajar_minigame/minigame_backend/controller/auth"
	gw_chat "gitlab.com/vajar_minigame/minigame_backend/data/chat"
)

var (
	// command-line options:
	// gRPC server endpoint
	grpcServerEndpoint = flag.String("grpc-server-endpoint", "localhost:5001", "gRPC server endpoint")
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Register gRPC server endpoint
	// Note: Make sure the gRPC server is running properly and accessible
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}

	err := gw_chat.RegisterChatServiceHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts)
	err = gw_auth.RegisterAuthHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts)
	err = gw_battle.RegisterBattleServiceHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts)
	if err != nil {
		return err
	}
	logrus.Info("Start listening on port 8081")
	// Start HTTP server (and proxy calls to gRPC server endpoint)
	return http.ListenAndServe(":8081", mux)
}

func main() {
	flag.Parse()
	flag.Set("v", "1")
	defer glog.Flush()

	if err := run(); err != nil {
		glog.Fatal(err)
	}
}
