module gitlab.com/vajar_minigame/gateway

go 1.12

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/protobuf v1.3.2
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.9.6
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/objx v0.2.0 // indirect
	gitlab.com/vajar_minigame/minigame_backend v0.0.0-20190825161616-fe5c7bd508c1
	golang.org/x/sys v0.0.0-20190826163724-acd9dae8e8cc // indirect
	golang.org/x/tools v0.0.0-20190826060629-95c3470cfb70 // indirect
	google.golang.org/grpc v1.23.0
)
